package com.yeah.y17611630829.art;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Notification;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.app.*;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.yeah.y17611630829.art.ui.login.LoginActivity;

/**
 * @Classname About
 * @Description 主界面
 * @Date 2021/2/5 11:59
 * @author yuanzhongze
 */
public class MainActivity extends AppCompatActivity {

    Button about;
    Button login;
    Button score;
    Button help;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //定位按钮
        about= (Button) findViewById(R.id.about);

        //添加监听器
        about.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, About.class);
                startActivity(intent);
            }
        });
        login= (Button) findViewById(R.id.login);

        login.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        });
        score= (Button) findViewById(R.id.score);

        score.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, score.class);
                startActivity(intent);
            }
        });
        help= (Button) findViewById(R.id.help);

        help.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {

                AlertDialog alertDialog2 = new AlertDialog.Builder(MainActivity.this)
                        .setTitle("帮助")
                        .setMessage("请先登录评委,然后点击'进入打分系统',输入0-10之间的分数")
                        .setIcon(R.mipmap.help)
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {//添加"Yes"按钮
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(MainActivity.this, "打分前请先登录", Toast.LENGTH_SHORT).show();
                            }
                        })
                        .create();
                alertDialog2.show();

            }
        });
    }
}
