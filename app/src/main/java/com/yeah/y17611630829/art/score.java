package com.yeah.y17611630829.art;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * @Classname About
 * @Description TODO
 * @Date 2021/2/5 11:59
 * @author yuanzhongze
 */
public class score extends AppCompatActivity {

    private Button Buttonback;
    private EditText edit;
    private Button updata;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        //定位按钮
        Buttonback= (Button) findViewById(R.id.Buttonback);
        updata= (Button) findViewById(R.id.updata);
        edit=(EditText)findViewById(R.id.editText);
        //添加监听器
        Buttonback.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(score.this, MainActivity.class);
                startActivity(intent);
               }
         });
        updata.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                String s=edit.getText().toString();
                //if(s==null||s.length()==0){
                if(TextUtils.isEmpty(s)){
                    edit.setError("输入不能为空");
                }
                else{
                    int i = Integer.parseInt(s);
                    if(i>10||i<0){
                        edit.setError("输入不合法");
                    }
                    else{
                        Toast.makeText(score.this, "提交成功!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(score.this, MainActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });
    }
}
