package com.yeah.y17611630829.art.data;

/**
 * @Classname About
 * @Description 一个泛型类，它包含一个带有数据或错误异常的结果
 * @Date 2021/2/5 11:59
 * @author yuanzhongze
 */
public class Result<T> {
    /** hide the private constructor to limit subclass types (Success, Error)*/
    private Result() {
    }

    @Override
    public String toString() {
        if (this instanceof Result.Success) {
            Result.Success success = (Result.Success) this;
            return "Success[data=" + success.getData().toString() + "]";
        } else if (this instanceof Result.Error) {
            Result.Error error = (Result.Error) this;
            return "Error[exception=" + error.getError().toString() + "]";
        }
        return "";
    }

    /** Success sub-class*/
    public final static class Success<T> extends Result {
        private T data;

        public Success(T data) {
            this.data = data;
        }

        public T getData() {
            return this.data;
        }
    }

    /** Error sub-class*/
    public final static class Error extends Result {
        private Exception error;

        public Error(Exception error) {
            this.error = error;
        }

        public Exception getError() {
            return this.error;
        }
    }
}
