package com.yeah.y17611630829.art.data;

import com.yeah.y17611630829.art.data.model.LoggedInUser;

import java.io.IOException;

 /**
 * @Classname About
 * @Description 该类处理带有登录凭据的身份验证并检索用户信息。
 * @Date 2021/2/5 11:59
 * @author yuanzhongze
 */
public class LoginDataSource {

    public Result<LoggedInUser> login(String username, String password) {

        try {
            // TODO: handle loggedInUser authentication
            LoggedInUser fakeUser =
                    new LoggedInUser(
                            java.util.UUID.randomUUID().toString(),
                            "Jane Doe");
            return new Result.Success<>(fakeUser);
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}
