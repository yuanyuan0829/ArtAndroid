package com.yeah.y17611630829.art.data.model;

/**
 * @Classname About
 * @Description 从LoginRepository检索到的已登录用户捕获用户信息的数据类
 * @Date 2021/2/5 11:59
 * @author yuanzhongze
 */
public class LoggedInUser {

    private String userId;
    private String displayName;

    public LoggedInUser(String userId, String displayName) {
        this.userId = userId;
        this.displayName = displayName;
    }

    public String getUserId() {
        return userId;
    }

    public String getDisplayName() {
        return displayName;
    }
}
