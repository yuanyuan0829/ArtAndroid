package com.yeah.y17611630829.art;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class Video extends AppCompatActivity {
    RotateAnimation rotateAnimation = null;
    LinearInterpolator linearInterpolator = null;
    ImageView imageView2;
    ImageView imageView3;
    ImageView imageView4;
    ImageView imageView5;
    TextView textView5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Button ButtonBack3;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        InitImage();
        imageView2.startAnimation(rotateAnimation);
        imageView3.startAnimation(rotateAnimation);
        imageView4.startAnimation(rotateAnimation);
        imageView5.startAnimation(rotateAnimation);
        //定位按钮
        ButtonBack3= (Button) findViewById(R.id.BackButton3);

        //添加监听器
        ButtonBack3.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Video.this, About.class);
                startActivity(intent);
            }
        });
    }
    private void InitImage(){

        imageView2 = findViewById(R.id.imageView2);
        imageView3 = findViewById(R.id.imageView3);
        imageView4 = findViewById(R.id.imageView4);
        imageView5 = findViewById(R.id.imageView5);

        rotateAnimation = new RotateAnimation(0,360, Animation.RELATIVE_TO_SELF,0.5f,Animation.RELATIVE_TO_SELF,0.5f);
        linearInterpolator = new LinearInterpolator();

        rotateAnimation.setDuration(2000);
        rotateAnimation.setRepeatCount(-1);
        rotateAnimation.setInterpolator(linearInterpolator);
    }
}
