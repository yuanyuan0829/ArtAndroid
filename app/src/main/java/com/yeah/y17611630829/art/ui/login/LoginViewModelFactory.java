package com.yeah.y17611630829.art.ui.login;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.annotation.NonNull;

import com.yeah.y17611630829.art.data.LoginDataSource;
import com.yeah.y17611630829.art.data.LoginRepository;


/**
 * @Classname About
 * @Description ViewModel提供对象来实例化LoginViewModel,LoginViewModel必须有一个非空构造函数
 * @Date 2021/2/5 11:59
 * @author yuanzhongze
 */
public class LoginViewModelFactory implements ViewModelProvider.Factory {

    @NonNull
    @Override
    @SuppressWarnings("unchecked")
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if (modelClass.isAssignableFrom(LoginViewModel.class)) {
            return (T) new LoginViewModel(LoginRepository.getInstance(new LoginDataSource()));
        } else {
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}
