# ArtDayApp
![](https://img.shields.io/badge/build-passing-brightgreen)  ![](https://img.shields.io/badge/license-%20%20LGPL--3.0-brightgreen "")  ![](https://img.shields.io/badge/platform-IOS%20%7C%20Android-lightgrey "")  ![](https://gitee.com/yuanyuan0829/ArtAndroid/badge/star.svg?theme=dark "")
#### 介绍
简易安卓投票器系统

#### 软件架构
单体架构


#### 安装教程

1.  下载安装

#### 使用说明

1.  打开软件
2.  登录
3.  进行打分

#### 下载链接
[最新构建](https://gitee.com/yuanyuan0829/ArtAndroid/releases)
